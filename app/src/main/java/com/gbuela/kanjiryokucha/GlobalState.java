package com.gbuela.kanjiryokucha;

public class GlobalState {

    private static GlobalState instance;

    public static GlobalState getInstance() {
        if (instance == null) {
            instance = new GlobalState();
        }
        return instance;
    }

    private boolean needsRefresh;

    private GlobalState() {
        needsRefresh = true;
    }

    public boolean isNeedsRefresh() {
        return needsRefresh;
    }

    public void setNeedsRefresh(boolean needsRefresh) {
        this.needsRefresh = needsRefresh;
    }
}
