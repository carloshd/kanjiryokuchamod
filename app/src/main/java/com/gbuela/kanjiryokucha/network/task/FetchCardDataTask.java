package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.Card;
import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.FetchCardsResult;
import com.gbuela.kanjiryokucha.network.KoohiiTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FetchCardDataTask extends KoohiiTask<FetchCardsResult> {

    public static int MAX_FETCH_CARDS = 10;

    private CardIdList cardIdList;
    private int startIndex;

    public FetchCardDataTask(CardIdList cardIdList, int startIndex) {
        super();
        this.cardIdList = cardIdList;
        this.startIndex = startIndex;
    }

    @Override
    protected HashMap<String, String> buildQuerystringParams() {
        final HashMap<String, String> qs = new HashMap<>();

        qs.put("yomi", "0");

        StringBuilder sb = new StringBuilder();
        for (int i = startIndex; i < cardIdList.getCardIds().size() && i < startIndex + MAX_FETCH_CARDS; i++) {
            if (i > startIndex) {
                sb.append(",");
            }
            sb.append(cardIdList.getCardIds().get(i));
        }

        qs.put("items", sb.toString());

        return qs;
    }

    @Override
    public String getApiMethod() {
        return "review/fetch";
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new FetchCardDataTask(this.cardIdList, this.startIndex);
    }

    @Override
    protected FetchCardsResult resultFromJson(JSONObject json) {
        FetchCardsResult result = new FetchCardsResult();
        result.setStartIndex(startIndex);
        try {
            JSONArray ja = json.getJSONArray("card_data");
            ArrayList<Card> cards = new ArrayList<>(ja.length());
            for (int i = 0; i < ja.length(); i++) {
                cards.add(Card.fromJson(ja.getJSONObject(i)));
            }
            result.setCards(cards);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
