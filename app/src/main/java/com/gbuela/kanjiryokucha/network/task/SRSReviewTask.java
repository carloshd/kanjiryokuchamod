package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.ReviewType;
import com.gbuela.kanjiryokucha.network.KoohiiTask;

import org.json.JSONObject;

import java.util.HashMap;

public class SRSReviewTask extends KoohiiTask<CardIdList> {

    private ReviewType type;

    public SRSReviewTask(ReviewType type) {
        super();
        this.type = type;
    }

    @Override
    public String getApiMethod() {
        return "review/start";
    }

    @Override
    protected CardIdList resultFromJson(JSONObject json) {
        return CardIdList.fromJson(json);
    }

    @Override
    protected HashMap<String, String> buildQuerystringParams() {
        final HashMap<String, String> qs = new HashMap<>();

        String qsType = "";
        switch (this.type) {
            case DUE:
                qsType = "due";
                break;
            case NEW:
                qsType = "new";
                break;
            case FAILED:
                qsType = "failed";
                break;
        }

        qs.put("mode", "srs");
        qs.put("type", qsType);

        return qs;
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new SRSReviewTask(this.type);
    }
}
