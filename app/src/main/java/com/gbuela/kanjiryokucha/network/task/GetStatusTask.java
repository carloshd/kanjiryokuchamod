package com.gbuela.kanjiryokucha.network.task;

import com.gbuela.kanjiryokucha.model.ReviewStatus;
import com.gbuela.kanjiryokucha.network.KoohiiTask;

import org.json.JSONObject;


public class GetStatusTask extends KoohiiTask<ReviewStatus> {

    @Override
    public String getApiMethod() {
        return "srs/info";
    }

    @Override
    protected ReviewStatus resultFromJson(JSONObject json) {
        return ReviewStatus.fromJson(json);
    }

    @Override
    protected KoohiiTask createTaskCopy() {
        return new GetStatusTask();
    }
}

