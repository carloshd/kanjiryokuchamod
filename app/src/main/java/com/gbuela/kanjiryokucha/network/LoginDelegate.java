package com.gbuela.kanjiryokucha.network;

public interface LoginDelegate {
    public void onLogin(boolean loggedIn);
}
