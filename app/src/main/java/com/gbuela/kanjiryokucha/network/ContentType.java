package com.gbuela.kanjiryokucha.network;

public enum ContentType {
    NONE,
    FORM,
    JSON
}
