package com.gbuela.kanjiryokucha.database;

import android.database.sqlite.SQLiteDatabase;

public interface WriteOperation {
    void execute(SQLiteDatabase db);
}
