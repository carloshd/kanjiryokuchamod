package com.gbuela.kanjiryokucha.database;

import android.provider.BaseColumns;

public final class ReviewContract {

    private ReviewContract() {}

    public static abstract class ReviewEntry implements BaseColumns {
        public static final String TABLE_NAME = "reviewentry";
        public static final String COLUMN_NAME_CARD_ID = "cardid";
        public static final String COLUMN_NAME_ANSWER = "answer";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

        // schema 2
        public static final String COLUMN_NAME_KEYWORD = "keyword";
        public static final String COLUMN_NAME_FRAMENUM = "framenum";
        public static final String COLUMN_NAME_STROKECOUNT = "strokes";

        private static final String TEXT_TYPE = " TEXT";
        private static final String INTEGER_TYPE = " INTEGER";
        private static final String COMMA_SEP = ",";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + ReviewEntry.TABLE_NAME + " (" +
                        ReviewEntry._ID + " INTEGER PRIMARY KEY," +
                        ReviewEntry.COLUMN_NAME_CARD_ID + INTEGER_TYPE + COMMA_SEP +
                        ReviewEntry.COLUMN_NAME_ANSWER + INTEGER_TYPE + COMMA_SEP +
                        ReviewEntry.COLUMN_NAME_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
                        " )";


        public static final String SQL_MIGRATE_1_2_KEYWORD =
                "ALTER TABLE " + ReviewEntry.TABLE_NAME +
                        " ADD " + ReviewEntry.COLUMN_NAME_KEYWORD + TEXT_TYPE + ";";
        public static final String SQL_MIGRATE_1_2_FRAMENUM  =
                "ALTER TABLE " + ReviewEntry.TABLE_NAME +
                        " ADD " + ReviewEntry.COLUMN_NAME_FRAMENUM + INTEGER_TYPE + " DEFAULT 0;";
        public static final String SQL_MIGRATE_1_2_STROKECOUNT  =
                "ALTER TABLE " + ReviewEntry.TABLE_NAME +
                        " ADD " + ReviewEntry.COLUMN_NAME_STROKECOUNT + INTEGER_TYPE + " DEFAULT 0;";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + ReviewEntry.TABLE_NAME;

    }
}
