package com.gbuela.kanjiryokucha.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gbuela.kanjiryokucha.GlobalState;
import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.database.KoohiiData;
import com.gbuela.kanjiryokucha.model.Card;
import com.gbuela.kanjiryokucha.model.CardAnswer;
import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.ReviewType;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.KoohiiTaskHandler;
import com.gbuela.kanjiryokucha.network.task.SRSReviewTask;
import com.gbuela.kanjiryokucha.network.task.SyncAnswersTask;

import java.util.ArrayList;
import java.util.List;

import static com.gbuela.kanjiryokucha.activity.KoohiiActivity.KoohiiConfirm;

public class SessionActivity extends KoohiiActivity implements View.OnClickListener, KoohiiConfirm.OKHandler {

    static final int CONFIRMTAG_CLEAR = 1;

    public static final int MAX_ANSWERS_POST = 50;

    private static final String TYPE_PARAM = "type";
    private static final String NUMCARDS_PARAM = "numCards";
    private static final String NEWSESSION_PARAM = "newSession";

    private static final String STATE_TYPE = "STATE_TYPE";
    private static final String STATE_NUMCARDS = "STATE_NUMCARDS";
    private static final String STATE_CARD_ID_LIST = "STATE_CARD_ID_LIST";

    private boolean mNewSession;
    private ReviewType mReviewType;
    private int mNumCards;
    private CardIdList mCardIdList;

    private View buttonToStudy;
    private View buttonToReview;
    private View buttonToSubmit;
    private Button buttonClear;
    private TextView textStudy;
    private TextView textReview;
    private TextView textSubmit;
    private ProgressBar progressBar;

    private KoohiiData data;

    public static Intent getSessionActivityIntent(Context context, boolean newSession, ReviewType reviewType, int numCards) {
        Intent intent = new Intent(context, SessionActivity.class);
        intent.putExtra(NEWSESSION_PARAM, newSession);
        intent.putExtra(TYPE_PARAM, reviewType.getCode());
        intent.putExtra(NUMCARDS_PARAM, numCards);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        buttonClear = (Button)findViewById(R.id.button_clear);
        buttonToStudy= findViewById(R.id.button_tostudy);
        buttonToReview = findViewById(R.id.button_toreview);
        buttonToSubmit = findViewById(R.id.button_tosubmit);
        textStudy = (TextView)findViewById(R.id.text_study_cards);
        textReview = (TextView)findViewById(R.id.text_review_cards);
        textSubmit = (TextView)findViewById(R.id.text_submit_cards);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        mNewSession = getIntent().getBooleanExtra(NEWSESSION_PARAM, false);
        mReviewType = ReviewType.typeFromInt(getIntent().getIntExtra(TYPE_PARAM, 0));
        mNumCards = getIntent().getIntExtra(NUMCARDS_PARAM, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_TYPE, mReviewType.getCode());
        outState.putInt(STATE_NUMCARDS, mNumCards);
        outState.putParcelable(STATE_CARD_ID_LIST, mCardIdList);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mReviewType = ReviewType.typeFromInt(savedInstanceState.getInt(STATE_TYPE));
        mNumCards = savedInstanceState.getInt(STATE_NUMCARDS);
        mCardIdList = savedInstanceState.getParcelable(STATE_CARD_ID_LIST);
        mNewSession = (mCardIdList == null || mCardIdList.getCardIds().size() == 0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getActionBar().setTitle(getString(R.string.current_session_title));

        final TextView textTitle = (TextView)this.findViewById(R.id.text_title);
        textTitle.setText(getReviewTypeDescription() + " " + getString(R.string.cards));
        textTitle.setBackgroundColor(getReviewTypeColor(true));

        buttonToReview.setBackgroundColor(getReviewTypeColor(false));
        buttonToSubmit.setBackgroundColor(getReviewTypeColor(false));

        buttonClear.setOnClickListener(this);
        buttonToStudy.setOnClickListener(this);
        buttonToReview.setOnClickListener(this);
        buttonToSubmit.setOnClickListener(this);

        data = new KoohiiData(this);

        if (mReviewType != ReviewType.FAILED) {
            findViewById(R.id.study_container).setVisibility(View.GONE);
        }

        if (mNewSession) {
            new SRSReviewTask(this.mReviewType).execute(this, new KoohiiTaskHandler<CardIdList>() {
                @Override
                public void handle(CardIdList cardIdList) {
                    setupNewReview(cardIdList);
                    updateUI();
                    mNewSession = false;
                }
            });
        }

        updateUI();
    }

    private CardIdList findStudiedIds() {
        final CardIdList cardIds = new CardIdList();

        final CardIdList unansweredIds = data.getUnanswaredCardIds();
        final CardIdList studyIds = data.getStudyCardIds();
        final ArrayList<Integer> studiedIds = new ArrayList<>();
        for (Integer u : unansweredIds.getCardIds()) {
            boolean found = false;
            for (Integer s : studyIds.getCardIds()) {
                if (u.equals(s)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                studiedIds.add(u);
            }
        }

        cardIds.setCardIds(studiedIds);
        return cardIds;
    }

    private void updateUI() {
        final long numAnswered = data.countAnsweredEntries();
        final long numStudyPending = data.countStudyPendingEntries();
        mNumCards = (int)data.countEntries();

        int numToReview = 0;
        if (mReviewType == ReviewType.FAILED) {
            numToReview = findStudiedIds().getCardIds().size();
        }
        else {
            numToReview =  (int) (mNumCards - numAnswered);
        }

        updateButton(buttonToStudy, textStudy, (int) numStudyPending);
        updateButton(buttonToReview, textReview, numToReview);
        updateButton(buttonToSubmit, textSubmit, (int) numAnswered);

        final TextView textTitle = (TextView)this.findViewById(R.id.text_title);
        textTitle.setText("" + mNumCards + " " + getReviewTypeDescription() + " " + getResources().getString(R.string.cards));
    }

    private void disableButtons() {
        buttonToStudy.setEnabled(false);
        buttonToReview.setEnabled(false);
        buttonToSubmit.setEnabled(false);
        buttonClear.setEnabled(false);
    }

    private void updateButton(View button, TextView text, int number) {
        text.setText("" + number + " " + getResources().getString(R.string.cards));
        button.setEnabled(number > 0);
        button.setBackgroundColor(getReviewTypeColor(number > 0));
    }

    private void setupNewReview(CardIdList cardIdList) {

        if (cardIdList.getCardIds().size() == 0) {
            Toast.makeText(this,
                    getString(R.string.no_cards_to_review_message), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        this.mCardIdList = cardIdList;
        data.initializeReview(mCardIdList, mReviewType);
    }

    private String getReviewTypeDescription() {
        switch(mReviewType) {
            case DUE:
                return "expired";
            case NEW:
                return "new";
            case FAILED:
                return "restudy";
        }
        return "(invalid type)";
    }

    private int getReviewTypeColor(boolean enabled) {
        switch(mReviewType) {
            case DUE:
                return getResources().getColor(enabled? R.color.koohiiOrange : R.color.koohiiOrangeDisabled);
            case NEW:
                return getResources().getColor(enabled? R.color.koohiiBlue : R.color.koohiiBlueDisabled);
            case FAILED:
                return getResources().getColor(enabled? R.color.koohiiRed : R.color.koohiiRedDisabled);
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        if (v == buttonClear) {
            final long numAnswered = data.countAnsweredEntries();
            if (numAnswered == 0) {
                data.clearSession();
                finish();
            }
            else {
                showConfirmationDialog(CONFIRMTAG_CLEAR, getString(R.string.clear_session_question_message), getString(R.string.there_are_unsubmitted_answers_message));
            }
        }
        else if (v == buttonToStudy) {
            final Intent intent = StudyActivity.getStudyActivityIntent(this);
            startActivity(intent);
        }
        else if (v == buttonToReview) {
            final CardIdList cardIdList = (mReviewType == ReviewType.FAILED)? findStudiedIds() : data.getUnanswaredCardIds();
            final Intent intent = ReviewActivity.getReviewActivityIntent(this, mReviewType, cardIdList);
            startActivity(intent);
        }
        else if (v == buttonToSubmit) {
            submit();
        }
    }

    private void submit() {

        final List<Card> cardAnswers = data.getAnswers(MAX_ANSWERS_POST);

        if (cardAnswers != null && cardAnswers.size() > 0) {
            SyncAnswersTask task = new SyncAnswersTask(cardAnswers);
            task.execute(this, new KoohiiTaskHandler<List<Integer>>() {
                @Override
                public void handle(final List<Integer> result) {
                    if (result != null) {
                        GlobalState.getInstance().setNeedsRefresh(true);
                        data.deleteSubmittedEntries(result);
                        updateUI();
                        for(Card card : cardAnswers) {
                            if (card.getAnswer() == CardAnswer.EASY || card.getAnswer() == CardAnswer.YES) {
                                data.deleteStudy(card);
                            }
                            else if (card.getAnswer() == CardAnswer.NO) {
                                data.insertStudy(card);
                            }
                        }
                        submit(); // will call recursively for next batch until all cards are sent or a call failed
                    }
                }
            });
        }
    }

    @Override
    public void userConfirmed(int tag) {
        if (tag == CONFIRMTAG_CLEAR) {
            data.clearSession();
            finish();
        }
    }

    @Override
    public void onTaskStart(KoohiiTask task) {
        progressBar.setVisibility(View.VISIBLE);
        disableButtons();
    }

    @Override
    public void onTaskEnd(KoohiiTask task) {
        progressBar.setVisibility(View.INVISIBLE);
        buttonClear.setEnabled(true);
    }
}
