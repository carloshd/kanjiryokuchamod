package com.gbuela.kanjiryokucha.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.database.KoohiiData;
import com.gbuela.kanjiryokucha.model.GlobalRecord;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.KoohiiTaskDelegate;
import com.gbuela.kanjiryokucha.network.LoginDelegate;
import com.gbuela.kanjiryokucha.network.LoginTask;
import com.gbuela.kanjiryokucha.utils.persistentcookies.PersistentCookieStore;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class KoohiiActivity extends Activity
        implements LoginDelegate,
        KoohiiTaskDelegate{

    private KoohiiSpinner progressDialog;
    private Dialog login;
    private KoohiiTask originalTask;
    private boolean automaticLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final PersistentCookieStore cookieStore = new PersistentCookieStore(this);
        final CookieManager cookieManager = new CookieManager(cookieStore, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
    }

    @Override
    protected void onDestroy() {
        if (login != null) {
            login.dismiss();
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        super.onDestroy();
    }



    protected void showLoginDialog() {

        if (login == null || !login.isShowing()) {
            login = new Dialog(this);
        }
        login.setContentView(R.layout.login_dialog);
        login.setTitle(getString(R.string.login_prompt));

        final Button btnLogin = (Button) login.findViewById(R.id.btnLogin);
        final Button btnCancel = (Button) login.findViewById(R.id.btnCancel);
        final EditText txtPassword = (EditText)login.findViewById(R.id.txtPassword);

        txtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    attemptAuthentication();
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAuthentication();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissLogin();
                KoohiiActivity.this.onLoginCancelled();
            }
        });

        login.show();
    }

    private void attemptAuthentication() {
        final EditText txtUsername = (EditText)login.findViewById(R.id.txtUsername);
        final EditText txtPassword = (EditText)login.findViewById(R.id.txtPassword);

        if(txtUsername.getText().toString().trim().length() > 0 && txtPassword.getText().toString().trim().length() > 0)
        {
            final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(txtUsername.getWindowToken(), 0);

            showSpinner(getString(R.string.logging_in_message), getString(R.string.please_wait_message));

            attemptAuthentication(txtUsername.getText().toString().trim(), txtPassword.getText().toString().trim());
        }
        else
        {
            Toast.makeText(KoohiiActivity.this,
                    getString(R.string.login_enter_credentials_message), Toast.LENGTH_LONG).show();
        }
    }

    private void attemptAuthentication(String username, String password) {
        final LoginTask task = new LoginTask(KoohiiActivity.this);
        String[] params = new String[2];
        params[0] = username;
        params[1] = password;

        task.execute(params);
    }

    protected void onLoginCancelled() {
        originalTask = null;
        Toast.makeText(this, "Login cancelled", Toast.LENGTH_LONG).show();
    }

    protected void dismissLogin() {
        try {
            if (login != null && login.isShowing()) {
                login.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // ignore
        } finally {
            login = null;
        }
    }

    protected void onLoggedIn() {
    }

    @Override
    public void onLogin(boolean loggedIn) {
        hideSpinner();

        final KoohiiData data = new KoohiiData(this);
        final GlobalRecord global = data.getGlobalRecord();

        if (loggedIn) {

            if (!automaticLogin) {
                final CheckBox checkStoreCred = (CheckBox) login.findViewById(R.id.checkStoreCred);
                if (checkStoreCred.isChecked()) {
                    final EditText txtUsername = (EditText) login.findViewById(R.id.txtUsername);
                    final EditText txtPassword = (EditText) login.findViewById(R.id.txtPassword);
                    global.setUsername(txtUsername.getText().toString());
                    global.setPassword(txtPassword.getText().toString());
                } else {
                    global.setUsername("");
                    global.setPassword("");
                }
                data.saveGlobalRecord(global);

                dismissLogin();
            }
            this.onLoggedIn();

            if (originalTask != null) {
                try {
                    final KoohiiTask newTask = (KoohiiTask)originalTask.clone();
                    newTask.execute();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
                originalTask = null;
            }
        }
        else {
            global.setUsername("");
            global.setPassword("");
            data.saveGlobalRecord(global);

            Toast.makeText(KoohiiActivity.this,
                    getString(R.string.login_failed_message), Toast.LENGTH_LONG).show();

            if (automaticLogin) {
                // For some reason automatic login failed -- ie changed password on koohii site
                // give up and pop up dialog for manual login
                automaticLogin = false;
                showLoginDialog();
            }
        }
    }

    protected void showConfirmationDialog(int tag, String title, String message) {
        final KoohiiConfirm confirmationDialog = KoohiiConfirm.newConfirm(tag, title, message);
        confirmationDialog.show(this.getFragmentManager(), "confirm");
    }

    protected void showSpinner(String title, String message) {
        progressDialog = KoohiiSpinner.newInstance(title, message);
        progressDialog.show(this.getFragmentManager(), "progress");
    }

    protected void hideSpinner() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    // Koohii task delegate

    @Override
    public void onNoActiveSession(KoohiiTask task) {
        originalTask = task;

        final KoohiiData data = new KoohiiData(this);
        final GlobalRecord global = data.getGlobalRecord();
        if (global.getUsername().length() > 0 && global.getPassword().length() > 0) {
            automaticLogin = true;
            attemptAuthentication(global.getUsername(), global.getPassword());
        }
        else {
            automaticLogin = false;
            hideSpinner();
            showLoginDialog();
        }
    }

    @Override
    public void onTaskFailure(KoohiiTask task, String message) {
        hideSpinner();
        Toast.makeText(this,
                message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTaskStart(KoohiiTask task) {

    }

    @Override
    public void onTaskEnd(KoohiiTask task) {

    }

    public static class KoohiiSpinner extends DialogFragment {

        private static final String PARAM_TITLE = "title";
        private static final String PARAM_MESSAGE = "message";

        public static KoohiiSpinner newInstance(String title, String message) {
            final Bundle args = new Bundle();
            args.putString(PARAM_TITLE, title);
            args.putString(PARAM_MESSAGE, message);
            final KoohiiSpinner koohiiProgress = new KoohiiSpinner();
            koohiiProgress.setArguments(args);
            return koohiiProgress;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            final ProgressDialog mProgress = new ProgressDialog(getActivity());
            mProgress.setTitle(getArguments().getString(PARAM_TITLE));
            mProgress.setMessage(getArguments().getString(PARAM_MESSAGE));
            mProgress.setCancelable(true);
            mProgress.setCanceledOnTouchOutside(false);
            return mProgress;
        }

        @Override
        public void onDestroyView() {
            if (getDialog() != null && getRetainInstance()) {
                getDialog().setDismissMessage(null);
            }
            super.onDestroyView();
        }
    }

    public static class KoohiiConfirm extends DialogFragment {

        public interface OKHandler {
            void userConfirmed(int tag);
        }

        private static final String PARAM_TITLE = "title";
        private static final String PARAM_MESSAGE = "message";
        private static final String PARAM_TAG = "tag";

        private int tag;
        private OKHandler okHandler;

        public static KoohiiConfirm newConfirm(int tag, String title, String message) {
            final Bundle args = new Bundle();
            args.putInt(PARAM_TAG, tag);
            args.putString(PARAM_TITLE, title);
            args.putString(PARAM_MESSAGE, message);
            final KoohiiConfirm koohiiConfirm = new KoohiiConfirm();
            koohiiConfirm.setArguments(args);
            return koohiiConfirm;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

            try {
                okHandler = (OKHandler)activity;
            }
            catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement OKHandler");
            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            tag = getArguments().getInt(PARAM_TAG);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle(getArguments().getString(PARAM_TITLE));
            alertDialogBuilder.setMessage(getArguments().getString(PARAM_MESSAGE));
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            okHandler.userConfirmed(tag);
                        }
                    });
            alertDialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            return alertDialogBuilder.create();
        }
    }
}
