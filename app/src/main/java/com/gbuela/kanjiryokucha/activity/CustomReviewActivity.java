package com.gbuela.kanjiryokucha.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gbuela.kanjiryokucha.R;
import com.gbuela.kanjiryokucha.model.CardIdList;
import com.gbuela.kanjiryokucha.model.ReviewType;
import com.gbuela.kanjiryokucha.network.KoohiiTask;
import com.gbuela.kanjiryokucha.network.KoohiiTaskHandler;
import com.gbuela.kanjiryokucha.network.task.ReviewRangeTask;

public class CustomReviewActivity extends KoohiiActivity implements View.OnClickListener {

    public static final int MAX_RANGE_SIZE = 100;

    private EditText editFrom;
    private EditText editTo;
    private CheckBox checkShuffle;
    private View buttonStart;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_review);

        editFrom = (EditText)findViewById(R.id.heisigFrom);
        editTo = (EditText)findViewById(R.id.heisigTo);
        checkShuffle = (CheckBox)findViewById(R.id.checkShuffle);
        buttonStart = findViewById(R.id.button_start);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        buttonStart.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getActionBar().setTitle(getString(R.string.custom_review_title));
    }

    @Override
    public void onClick(View v) {
        try {
            if (v == buttonStart) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editFrom.getWindowToken(), 0);

                int from = Integer.parseInt(editFrom.getText().toString());
                int to = Integer.parseInt(editTo.getText().toString());

                if (from <= 0 || to <= 0) {
                    Toast.makeText(this,
                            getString(R.string.enter_valid_range_message), Toast.LENGTH_LONG).show();
                    return;
                }

                if (from > to) {
                    final int swap = from;
                    from = to;
                    to = swap;
                }

                if ((1 + (to - from)) > MAX_RANGE_SIZE) {
                    Toast.makeText(this,
                            "Range too big! maximum size is " + MAX_RANGE_SIZE, Toast.LENGTH_LONG).show();
                    return;
                }

                new ReviewRangeTask(from, to, checkShuffle.isChecked()).execute(this, new KoohiiTaskHandler<CardIdList>() {
                    @Override
                    public void handle(CardIdList cardIdList) {
                        final Intent intent = ReviewActivity.getReviewActivityIntent(CustomReviewActivity.this, ReviewType.FREE_RANGE, cardIdList);
                        startActivity(intent);
                    }
                });

            }
        }
        catch (NumberFormatException e) {
            Toast.makeText(this,
                    "Please enter valid index numbers", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onTaskStart(KoohiiTask task) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskEnd(KoohiiTask task) {
        progressBar.setVisibility(View.INVISIBLE);
    }
}
