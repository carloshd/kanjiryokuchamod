/**
 The MIT License (MIT)

 Copyright (c) 2014 Niko Yuwono

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 **/

/**
  KoohiiProgress by Germán Buela is loosely based on Niko Yuwono's CustomProgress
 **/

package com.gbuela.kanjiryokucha.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.gbuela.kanjiryokucha.R;

import java.util.Arrays;

public class KoohiiProgress extends RelativeLayout {
    private int mMaximumValue;
    private int mCurrentValue;

    private int progressColor;
    private int progressBackgroundColor;
    private ShapeDrawable progressDrawable;
    private int width = 0;
    private int maxWidth = 0;
    private float cornerRadius = 25.0f;

    public KoohiiProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        progressColor = getResources().getColor(R.color.koohiiProgress);
        progressBackgroundColor = getResources().getColor(R.color.koohiiProgressBackground);
        mMaximumValue = 10;
        mCurrentValue = 0;
    }

    public int getMaximumValue() {
        return mMaximumValue;
    }

    public void setMaximumValue(int mMaximumValue) {
        this.mMaximumValue = mMaximumValue;
        invalidate();
        requestLayout();
    }

    public int getCurrentValue() {
        return mCurrentValue;
    }

    public void setCurrentValue(int mCurrentValue) {
        this.mCurrentValue = mCurrentValue;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        this.maxWidth = ((this.getWidth() / getMaximumValue()) * getCurrentValue());
        if(changed) {
            initView();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        progressDrawable.setBounds(0, 0, width, this.getHeight());
        progressDrawable.draw(canvas);

        if(width < maxWidth) {
            width +=5;
            invalidate();
        }
    }

    /**
     * Initialize the view before it will be drawn
     */
    private void initView() {
        final float[] outerRadius = new float[8];
        Arrays.fill(outerRadius, cornerRadius);
        final Shape progressShapeDrawable = new RoundRectShape(outerRadius, null, null);
        final Shape backgroundProgressShapeDrawable = new RoundRectShape(outerRadius, null, null);

        //Progress
        progressDrawable = new ShapeDrawable(progressShapeDrawable);
        progressDrawable.getPaint().setColor(progressColor);

        //Background
        final ShapeDrawable backgroundDrawable = new ShapeDrawable(backgroundProgressShapeDrawable);
        backgroundDrawable.getPaint().setColor(progressBackgroundColor);
        backgroundDrawable.setBounds(0, 0, this.getWidth(), this.getHeight());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.setBackground(backgroundDrawable);
        } else {
            this.setBackgroundDrawable(backgroundDrawable);
        }

        this.maxWidth = ((this.getWidth() / getMaximumValue()) * getCurrentValue());
    }

}
