package com.gbuela.kanjiryokucha.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DrawingView extends View {

    private Paint paint = new Paint();
    private SerializablePath path = new SerializablePath();
    private boolean drawable;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint.setAntiAlias(true);
        paint.setStrokeWidth(5f);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);

        setDrawable(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(path != null) {
            canvas.drawPath(path, paint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isDrawable()) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    return false;
            }

            invalidate();
            return true;
        }
        else {
            return false;
        }
    }

    public void save() {
        FileWriter writer = null;
        try {
            final JSONArray paths = path.serialize();
            final File file = new File(getContext().getCacheDir(), "drawing");
            writer = new FileWriter(file);
            writer.append(paths.toString());
            writer.flush();
        } catch (Exception e) {
            int i = 1;
            // fail silently
        }
        finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    // fail silently
                }
            }
        }
    }
    // FIXME: doesn't work for rotation - different canvas size, path not scaled
    public void restore() {
        BufferedReader br = null;
        try {
            final File file = new File(getContext().getCacheDir(), "drawing");
            br = new BufferedReader(new FileReader(file));
            final StringBuilder text = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            final String data = text.toString();
            final JSONArray paths = new JSONArray(data);
            path.deserialize(paths);
            invalidate();
        }
        catch (Exception e) {
            // fail silently
        }
        finally {
            if(br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // fail silently
                }
            }
        }
    }

    public SerializablePath getPath()
    {
        return path;
    }

    public void setPath(SerializablePath path)
    {
        this.path = path;
    }

    public void clear() {
        this.path = new SerializablePath();
        invalidate();
    }

    public boolean isDrawable() {
        return drawable;
    }

    public void setDrawable(boolean drawable) {
        this.drawable = drawable;
    }
}


