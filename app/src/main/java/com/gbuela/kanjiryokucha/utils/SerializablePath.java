package com.gbuela.kanjiryokucha.utils;

import android.graphics.Path;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SerializablePath extends Path {

    private List<Action> actions = new ArrayList<>();

    public void deserialize(JSONArray array) throws JSONException {
        actions = new ArrayList<>();
        if (array != null) {
            for (int i=0;i<array.length();i++){
                final JSONObject object = (JSONObject)array.get(i);
                final Action action = Action.deserialize(object);
                if(action != null) {
                    action.perform(this);
                }
            }
        }
    }

    public JSONArray serialize() throws JSONException {
        final JSONArray array = new JSONArray();
        final ListIterator<Action> listIterator = actions.listIterator();
        while(listIterator.hasNext()) {
            final Action action = listIterator.next();
            array.put(action.serialize());
        }
        return array;
    }

    @Override
    public void lineTo(float x, float y) {
        actions.add(new Line(x, y));
        super.lineTo(x, y);
    }

    @Override
    public void moveTo(float x, float y) {
        actions.add(new Move(x, y));
        super.moveTo(x, y);
    }

    private static abstract class Action {
        float x, y;

        abstract void perform(Path path);

        abstract String getActionType();

        JSONObject serialize() {
            try {
                final JSONObject json = new JSONObject();
                json.put("t", getActionType());
                json.put("x", x);
                json.put("y", y);
                return json;
            }
            catch (JSONException e) {
                return null;
            }
        }

        static Action deserialize(JSONObject json) {
            try {
                final float x = (float)json.getDouble("x");
                final float y = (float)json.getDouble("y");
                final Action action = json.get("t").equals("m") ? new Move(x, y) : new Line(x, y);
                return action;
            }
            catch (JSONException e) {
                return null;
            }
        }
    }

    private static final class Move extends Action {

        public Move(float x, float y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void perform(Path path) {
            path.moveTo(x, y);
        }

        @Override
        String getActionType() {
            return "m";
        }
    }

    private static final class Line extends Action {

        public Line(float x, float y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void perform(Path path) {
            path.lineTo(x, y);
        }

        @Override
        String getActionType() {
            return "l";
        }
    }
}