package com.gbuela.kanjiryokucha.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CardIdList implements Parcelable {

    private ArrayList<Integer> mCardIds;

    public static CardIdList fromJson(JSONObject jo) {
        CardIdList cl = new CardIdList();

        try {
            JSONArray ja = jo.getJSONArray("items");
            ArrayList<Integer> cardIds = new ArrayList<>(ja.length());
            for (int i = 0; i < ja.length(); i++) {
                cardIds.add(ja.getInt(i));
            }
            cl.setCardIds(cardIds);
        } catch (JSONException e) {
            return null;
        }

        return cl;
    }

    public CardIdList(Parcel in) {
        final int size = in.readInt();
        mCardIds = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            mCardIds.add(in.readInt());
        }
    }

    public CardIdList() {

    }

    public ArrayList<Integer> getCardIds() {
        return mCardIds;
    }

    public void setCardIds(ArrayList<Integer> cardIds) {
        this.mCardIds = cardIds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCardIds.size());
        for (int i = 0; i < mCardIds.size(); i++) {
            dest.writeInt(mCardIds.get(i));
        }
    }

    public static final Parcelable.Creator<CardIdList> CREATOR
            = new Parcelable.Creator<CardIdList>() {

        @Override
        public CardIdList createFromParcel(Parcel in) {
            return new CardIdList(in);
        }

        @Override
        public CardIdList[] newArray(int size) {
            return new CardIdList[size];
        }
    };
}
