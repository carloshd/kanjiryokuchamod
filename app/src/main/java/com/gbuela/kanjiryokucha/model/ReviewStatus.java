package com.gbuela.kanjiryokucha.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ReviewStatus {

    private int numberDueCards;
    private int numberNewCards;
    private int numberFailedCards;

    public static ReviewStatus fromJson(JSONObject jo) {
        ReviewStatus rs = new ReviewStatus();

        try {
            rs.setNumberDueCards(jo.getInt("due_cards"));
            rs.setNumberNewCards(jo.getInt("new_cards"));
            rs.setNumberFailedCards(jo.getInt("relearn_cards"));
        } catch (JSONException e) {
            return null;
        }

        return rs;
    }

    public int getNumberDueCards() {
        return numberDueCards;
    }

    public void setNumberDueCards(int numberDueCards) {
        this.numberDueCards = numberDueCards;
    }

    public int getNumberNewCards() {
        return numberNewCards;
    }

    public void setNumberNewCards(int numberNewCards) {
        this.numberNewCards = numberNewCards;
    }

    public int getNumberFailedCards() {
        return numberFailedCards;
    }

    public void setNumberFailedCards(int numberFailedCards) {
        this.numberFailedCards = numberFailedCards;
    }
}
