package com.gbuela.kanjiryokucha.model;

public enum CardAnswer {
    UNANSWERED(0),
    NO(1),
    YES(2),
    EASY(3),
    DELETE(4),
    SKIP(5),
    HARD(6);

    protected int code;

    CardAnswer(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public String getStringCode() {
        if (this == HARD) {
            return "h";
        }
        return null;
    }

    public static CardAnswer cardAnswerFromInt(int code) {
        switch (code) {
            case 1: return NO;
            case 2: return YES;
            case 3: return EASY;
            case 4: return DELETE;
            case 5: return SKIP;
            case 6: return HARD;
            default: return UNANSWERED;
        }
    }
}
