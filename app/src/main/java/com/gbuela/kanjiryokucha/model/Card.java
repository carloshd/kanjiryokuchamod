package com.gbuela.kanjiryokucha.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Card implements Parcelable {
    private int mKanjiId;
    private String mKeyword;
    private int mStrokeCount;
    private int mFrameNum;
    private CardAnswer mAnswer;
    private long mAnswerTimestamp;

    public Card() {
        this.setAnswer(CardAnswer.UNANSWERED);
    }

    public static Card fromJson(JSONObject jo) {
        Card card = new Card();

        try {
            card.setKanjiId(jo.getInt("id"));
            card.setKeyword(jo.getString("keyword"));
            card.setStrokeCount(jo.getInt("strokecount"));
            card.setFrameNum(jo.getInt("framenum"));

        } catch (JSONException e) {
            return null;
        }

        return card;
    }

    public Card(Parcel in) {
        mKanjiId = in.readInt();
        mKeyword = in.readString();
        mStrokeCount = in.readInt();
        mFrameNum = in.readInt();
        mAnswer = CardAnswer.cardAnswerFromInt(in.readInt());
        mAnswerTimestamp = in.readLong();
    }

    public String getKeyword() {
        return mKeyword;
    }

    public void setKeyword(String keyword) {
        this.mKeyword = keyword;
    }

    public int getKanjiId() {
        return mKanjiId;
    }

    public void setKanjiId(int kanjiId) {
        this.mKanjiId = kanjiId;
    }

    public CardAnswer getAnswer() {
        return mAnswer;
    }

    public void setAnswer(CardAnswer answer) {
        this.mAnswer = answer;
    }

    public int getStrokeCount() {
        return mStrokeCount;
    }

    public void setStrokeCount(int strokeCount) {
        this.mStrokeCount = strokeCount;
    }

    public int getFrameNum() {
        return mFrameNum;
    }

    public void setFrameNum(int frameNum) {
        this.mFrameNum = frameNum;
    }

    public long getmAnswerTimestamp() {
        return mAnswerTimestamp;
    }

    public void setmAnswerTimestamp(long mAnswerTimestamp) {
        this.mAnswerTimestamp = mAnswerTimestamp;
    }

    @Override
    public String toString() {
        return mKeyword != null? mKeyword : "id: " + mKanjiId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mKanjiId);
        dest.writeString(mKeyword);
        dest.writeInt(mStrokeCount);
        dest.writeInt(mFrameNum);
        dest.writeInt(mAnswer.getCode());
        dest.writeLong(mAnswerTimestamp);
    }

    public static final Parcelable.Creator<Card> CREATOR
            = new Parcelable.Creator<Card>() {

        @Override
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

}
