package com.gbuela.kanjiryokucha.readings;

import android.content.Context;
import android.os.AsyncTask;

import com.gbuela.kanjiryokucha.model.Yomi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class LoadReadingsTask extends AsyncTask<Void, Void, HashMap<Integer,Yomi>> {

    public static final String READINGS_FILENAME = "readings.txt";
    public static final String NANORI_MARKER = "Name Readings:  ";
    public static final String ELEMENT_SEPARATOR = "    ";

    private LoadReadingsDelegate delegate;

    private Context context;

    public void execute(Context context, LoadReadingsDelegate delegate) {
        this.context = context;
        this.delegate = delegate;
        execute();
    }

    @Override
    protected HashMap<Integer, Yomi> doInBackground(Void... params) {
        final HashMap<Integer, Yomi> readings = new HashMap<>();

        InputStream is = null;

        try {
            is = this.context.getAssets().open(READINGS_FILENAME);
            final InputStreamReader isr = new InputStreamReader(is);
            final BufferedReader br = new BufferedReader(isr);

            String line;
            while((line = br.readLine()) != null) {
                final Yomi yomi = extractReadings(line);
                if (yomi != null) {
                    readings.put(yomi.getKanjiId(), yomi);
                }
            }
        }
        catch (Exception e) {
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return readings;
    }

    private Yomi extractReadings(String line) {
        final String[] mainParts = line.split("\t");
        if (mainParts.length != 2) {
            return null;
        }
        final Yomi yomi = new Yomi();
        final ArrayList<String> onAndKun = new ArrayList<>();

        final int id = Integer.parseInt(mainParts[0], 16);
        yomi.setKanjiId(id);

        final String[] parts = mainParts[1].split(ELEMENT_SEPARATOR);

        if (parts.length < 2) {
            return null;
        }

        int i = 0;
        while(i < parts.length && !parts[i].startsWith(NANORI_MARKER)) {
            onAndKun.add(parts[i++]);
        }
        yomi.setReadings(onAndKun);

        if (i < parts.length) {
            parts[i] = parts[i].substring(NANORI_MARKER.length());
            final ArrayList<String> nanori = new ArrayList<>();
            while(i < parts.length) {
                nanori.add(parts[i++]);
            }
            yomi.setNanori(nanori);
        }

        return yomi;
    }

    @Override
    protected void onPostExecute(HashMap<Integer, Yomi> allReadings) {
        super.onPostExecute(allReadings);
        this.delegate.onLoadedReadings(allReadings);
    }
}
